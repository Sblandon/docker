FROM sbt783/sbt783:helloWorld
COPY HelloWorld /HelloWorld
WORKDIR /HelloWorld/
RUN gcc -o HelloWorld main.c
CMD ["./HelloWorld"]